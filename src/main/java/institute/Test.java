/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package institute;

/**
 *
 * @author jayvy
 */
import java.util.*;
public class Test {
    public static void main(String [] args){
     Student s1 = new Student("John", 1, "CSE");
     Student s2 = new Student("Maria", 2, "CSE");   
     Student s3 = new Student("Rahul", 2,"EE");   
     Student s4 = new Student("Priya", 2, "CSE");   
        
     List<Student>cse_students = new ArrayList<Student>();
     cse_students.add(s3);
     cse_students.add(s4);
     
     List<Student> ee_students = new ArrayList<Student>();
     ee_students.add(s3);
     ee_students.add(s4);
     
     Department CSE = new Department("CSE", cse_students);
     Department EE = new Department("EE", ee_students);
     
     List <Department> departments = new ArrayList<Department>();
     departments.add(CSE);
     departments.add(EE);
     
     Institute institute = new Institute ("BITS", departments);
     
     
     System.out.println("Total students in institute: ");
     System.out.print(institute.getTotalStudentsInInstitute());
        
        
        
        
    }
    
}
